<?php

class Meanbee_FooterJsCss_Helper_Css extends Meanbee_FooterJsCss_Helper_Data
{
    // Regular expression that matches one or more script tags (including conditions but not comments)
    const REGEX_CSS           = '#(\s*<!--(\[if[^\n]*>)?\s*(<link rel="stylesheet".*/>)+\s*(<!\[endif\])?-->)|(\s*<link type="text/css".*/>)#isU';
    const REGEX_DOCUMENT_END  = '#</body>\s*</html>#isU';

    const XML_CONFIG_FOOTER_CSS_ENABLED = 'dev/css/meanbee_footer_css_enabled';
    const XML_CONFIG_FOOTER_CSS_EXCLUDED_BLOCKS = 'dev/css/meanbee_footer_css_excluded_blocks';
    const XML_CONFIG_FOOTER_CSS_EXCLUDED_FILES = 'dev/css/meanbee_footer_css_excluded_files';

    const EXCLUDE_FLAG_CSS = 'data-footer-css-skip="true"';
    const EXCLUDE_FLAG_PATTERN_CSS = 'data-footer-css-skip';

    /** @var array */
    protected $_blocksToExclude;

    /** @var string */
    protected $skippedFilesRegex;

    /**
     * @param null $store
     *
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_CONFIG_FOOTER_CSS_ENABLED, $store);
    }

    public function removeCss($html)
    {
        $patterns = array(
            'css'             => self::REGEX_CSS
        );

        foreach($patterns as $pattern) {
            $matches = array();

            $success = preg_match_all($pattern, $html, $matches);
            if ($success) {
                foreach ($matches[0] as $key => $js) {
                    if (strpos($js, self::EXCLUDE_FLAG_PATTERN_CSS) !== false) {
                        unset($matches[0][$key]);
                    }
                }
                $html = str_replace($matches[0], '', $html);
            }
        }

        return $html;
    }

    public function moveCssToEnd($html)
    {
        $patterns = array(
            'css'             => self::REGEX_CSS,
            'document_end'   => self::REGEX_DOCUMENT_END
        );
//print_R($patterns);die;
        foreach($patterns as $pattern) {
            $matches = array();

            $success = preg_match_all($pattern, $html, $matches);
            if ($success) {
                // Strip excluded files
                if ($this->getSkippedFilesRegex() !== false) {
                    $matches[0] = preg_grep($this->getSkippedFilesRegex(), $matches[0], PREG_GREP_INVERT);
                }
                foreach ($matches[0] as $key => $js) {
                    if (strpos($js, self::EXCLUDE_FLAG_PATTERN_CSS) !== false) {
                        unset($matches[0][$key]);
                    }
                }
                $text = implode('', $matches[0]);
                $html = str_replace($matches[0], '', $html);
                $html = $html . $text;
            }
        }

        return $html;
    }

    protected function getSkippedFilesRegex()
    {
        if ($this->skippedFilesRegex === null) {
            $skipConfig = trim(Mage::getStoreConfig(self::XML_CONFIG_FOOTER_CSS_EXCLUDED_FILES));
            if ($skipConfig !== '') {
                $skippedFiles = preg_replace('/\s*,\s*/', '|', $skipConfig);
                $this->skippedFilesRegex = sprintf("@href=.*?(%s)@", $skippedFiles);
            } else {
                $this->skippedFilesRegex = false;
            }
        }
        return $this->skippedFilesRegex;
    }

    public function addCssToExclusion($html)
    {
        return str_replace('<link rel="stylesheet"', '<link rel="stylesheet" ' . self::EXCLUDE_FLAG_CSS, $html);
    }

    /**
     * Get list of block names (in layout) to exclude their JS from moving to footer
     *
     * @return array
     */

    public function getBlocksToSkipMoveCss()
    {
        if (is_null($this->_blocksToExclude)) {
            $string = Mage::getStoreConfig(self::XML_CONFIG_FOOTER_CSS_EXCLUDED_BLOCKS);
            $excludedBlocks = explode(',', $string);
            foreach ($excludedBlocks as $key => $blockName) {
                $excludedBlocks[$key] = trim($blockName);
                if (strpos($excludedBlocks[$key], "\n") || strpos($excludedBlocks[$key], ' ')) {
                    Mage::log('Missing comma in setting "' . self::XML_CONFIG_FOOTER_CSS_EXCLUDED_BLOCKS . '"', Zend_Log::ERR, null, true);
                }
            }
            $this->_blocksToExclude = array_filter($excludedBlocks);
        }
        return $this->_blocksToExclude;
    }
}
