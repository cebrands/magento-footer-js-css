<?php

class Meanbee_FooterJsCss_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function handleBlockInlineJsCss(Varien_Event_Observer $observer)
    {
        Varien_Profiler::start('MeanbeeFooterJs');
        $this->handleBlockInlineJs($observer);
        Varien_Profiler::stop('MeanbeeFooterJs');

        Varien_Profiler::start('MeanbeeFooterCss');
        $this->handleBlockInlineCss($observer);
        Varien_Profiler::stop('MeanbeeFooterCss');

        return $this;
    }

    protected function handleBlockInlineJs($observer)
    {
        $helper = Mage::helper('meanbee_footer_js_css/js');
        if (!$helper->isEnabled()) {
            return $this;
        }

        /** @var Varien_Object $transport */
        $transport = $observer->getTransport();

        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getBlock();

        if (in_array($block->getNameInLayout(), $helper->getBlocksToSkipMoveJs())) {
            $transport->setHtml($helper->addJsToExclusion($transport->getHtml()));
        }

        if (Mage::app()->getRequest()->getModuleName() == 'pagecache') {
            $transport->setHtml($helper->removeJs($transport->getHtml()));
            return $this;
        }

        if (!is_null($block->getParentBlock())) {
            // Only look for JS at the root block
            return $this;
        }

        $transport->setHtml($helper->moveJsToEnd($transport->getHtml()));
    }

    protected function handleBlockInlineCss($observer)
    {
        $helper = Mage::helper('meanbee_footer_js_css/css');
        if (!$helper->isEnabled()) {
            return $this;
        }

        /** @var Varien_Object $transport */
        $transport = $observer->getTransport();

        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getBlock();

        if (in_array($block->getNameInLayout(), $helper->getBlocksToSkipMoveCss())) {
            $transport->setHtml($helper->addCssToExclusion($transport->getHtml()));
        }

        if (Mage::app()->getRequest()->getModuleName() == 'pagecache') {
            $transport->setHtml($helper->removeCss($transport->getHtml()));
            return $this;
        }

        if (!is_null($block->getParentBlock())) {
            // Only look for CSS at the root block
            return $this;
        }

        $transport->setHtml($helper->moveCssToEnd($transport->getHtml()));
    }
}
